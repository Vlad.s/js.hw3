'use strict';
let number = /\d/g;
let notNumber = /\D/g;

function result (a, b, selector) {
    if (selector === '+'){
        return +a + +b;
    }
    if (selector === "-"){
        return +a - +b;
    }
    if (selector === "*"){
        return +a * +b;
    }
    if (selector === "/"){
        return +a / +b;
    }
}

let num1 = prompt('Enter the number one!', 'Number-1');
while (num1.search(number) === -1 || num1.search(notNumber) !== -1){
    num1 = prompt('Enter the number one!', `${num1}`);
}
let num2 = prompt('Enter the number two!', 'Number-2');
while (num2.search(number) === -1 || num2.search(notNumber) !== -1){
    num2 = prompt('Enter the number two!', `${num2}`);
}
let operation = prompt ('What to do with the numbers? ', '"+"  "-"   "*"   "/"');

console.log(result(num1, num2, operation));